﻿namespace Coppy_Virrus_Webb.Utility
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Configuration;

    using Serilog;

    /// <summary>
    /// Generic static helper.
    /// </summary>
    public static class GenericHelper
    {
        /// <summary>
        /// Gets or sets path to appsettings.json.
        /// </summary>
        public static string PathToSettings { get; set; }

        /// <summary>
        /// Gets or sets application configuration.
        /// </summary>
        public static IConfiguration Config { get; set; }

        /// <summary>
        /// See:
        /// https://stackoverflow.com/questions/31929482/retrieve-sections-from-config-json-in-asp-net-5
        /// </summary>
        /// <param name="config">Application configuration.</param>
        /// <param name="stripSectionPath">Strip section path.</param>
        /// <returns>Dictionary of strings.</returns>
        public static Dictionary<string, string> ConfigToDictionary(this IConfiguration config, bool stripSectionPath = true)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            using (
                new IndentedTraceGroup(
                    TraceSettings.CoppyVirrusWebbTraceLevelSwitch.TraceInfo,
                    $"GenericHelper: ConfigToDictionary",
                    "/// GenericHelper: ConfigToDictionary"))
            {
                var data = new Dictionary<string, string>();
                var section = stripSectionPath ? config as IConfigurationSection : null;
                ConvertToDictionary(config, data, section);
                return data;
            }
        }

        /// <summary>
        /// Reads resource as byte array.
        /// </summary>
        /// <param name="resource">Name of the resource to read.</param>
        /// <returns>Byte array of read data.</returns>
        public static byte[] ReadEmbeddedResource(string resource)
        {
            var a = System.Reflection.Assembly.GetExecutingAssembly();
            using (var resFilestream = a.GetManifestResourceStream(resource))
            {
                if (resFilestream == null)
                {
                    return null;
                }

                var ba = new byte[resFilestream.Length];
                resFilestream.Read(ba, 0, ba.Length);
                return ba;
            }
        }

        /// <summary>
        /// Gets all inner exceptions.
        /// </summary>
        /// <param name="ex">Exception to extract messages from.</param>
        /// <returns>All messegaes from inner exceptions.</returns>
        public static string FlattenException(Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException(nameof(ex));
            }

            using (
                new IndentedTraceGroup(
                    TraceSettings.CoppyVirrusWebbTraceLevelSwitch.TraceInfo,
                    $"GenericHelper: FlattenException",
                    "/// GenericHelper: FlattenException"))
            {
                var exceptionMessages = new StringBuilder();
                do
                {
                    exceptionMessages.Append(exceptionMessages.Length == 0 ? string.Empty : "\n" + ex.Message);
                    ex = ex.InnerException;
                }
                while (ex != null);

                return exceptionMessages.ToString();
            }
        }

        /// <summary>
        /// Converts config section to list.
        /// </summary>
        /// <param name="section">
        /// The section.
        /// </param>
        /// <returns>
        /// Dictionary of strings.
        /// </returns>
        public static List<string> ConfigSectionToList(IConfigurationSection section)
        {
            if (section == null)
            {
                throw new ArgumentNullException(nameof(section));
            }

            using (
                new IndentedTraceGroup(
                    TraceSettings.CoppyVirrusWebbTraceLevelSwitch.TraceInfo,
                    $"GenericHelper: ConfigSectionToList",
                    "/// GenericHelper: ConfigSectionToList"))
            {
                var children = section.GetChildren();
                return (from child in children where child.Value != null select child.Value).ToList();
            }
        }

        /// <summary>
        /// Generates random string.
        /// </summary>
        /// <param name="length">Length of generated string.</param>
        /// <returns>Random string of specified length.</returns>
        public static string RandomString(int length)
        {
            var random = new Random();
            const string Chars = "adefhijkmnoprtuvwxyzABDEFHJKMNPQRTWXY3478";
            return new string(Enumerable.Repeat(Chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Checks validity of the phone number.
        /// </summary>
        /// <param name="phoneNumber">Phone number to check.</param>
        /// <returns>True if phone number is valid, false otherwise.</returns>
        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            return true;
        }

        /// <summary>
        /// Gets an http request.
        /// </summary>
        /// <param name="uri">Uri to request.</param>
        /// <returns>Html response.</returns>
        public static async Task<string> GetRequestHtml(Uri uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException(nameof(uri));
            }

            using (
                new IndentedTraceGroup(
                    TraceSettings.CoppyVirrusWebbTraceLevelSwitch.TraceInfo,
                    $"GenericHelper: GetRequestHtml",
                    "/// GenericHelper: GetRequestHtml"))
            {
                using (var client = new HttpClient())
                {
                    // equivalent to pressing the submit button on
                    // a form with attributes (action="{url}" method="post")
                    HttpResponseMessage response = null;
                    try
                    {
                        response = await client.GetAsync(uri).ConfigureAwait(false);
                    }
                    catch (AggregateException aggregEx)
                    {
                        var flat = FlattenException(aggregEx);
                        Log.Error(aggregEx, flat);
                    }

                    if (response != null && !response.IsSuccessStatusCode)
                    {
                        Log.Warning(
                            "GetRequestHtml failed (" + response.StatusCode.ToString() + ": "
                            + response.ReasonPhrase + ") (uri: " + uri + ");");
                    }
                    else
                    {
                        Log.Information("GetRequestHtml succeeded (uri: " + uri + ");");
                    }

                    if (response != null)
                    {
                        var ret = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return ret;
                    }

                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Converts configuration section to dictionary.
        /// </summary>
        /// <param name="config">Application configuration.</param>
        /// <param name="data">Data dictionary.</param>
        /// <param name="top">Top level section.</param>
        private static void ConvertToDictionary(IConfiguration config, Dictionary<string, string> data = null, IConfigurationSection top = null)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            if (data == null)
            {
                data = new Dictionary<string, string>();
            }

            using (
                new IndentedTraceGroup(
                    TraceSettings.CoppyVirrusWebbTraceLevelSwitch.TraceInfo,
                    $"GenericHelper: ConvertToDictionary",
                    "/// GenericHelper: ConvertToDictionary"))
            {
                var children = config.GetChildren();
                foreach (var child in children)
                {
                    if (child.Value == null)
                    {
                        ConvertToDictionary(config.GetSection(child.Key), data);
                        continue;
                    }

                    var key = top != null ? child.Path.Substring(top.Path.Length + 1) : child.Path;
                    data[key] = child.Value;
                }
            }
        }

        /// <summary>
        /// Validates e-mail address.
        /// </summary>
        /// <param name="email">E-mail address to validate.</param>
        /// <returns>True if valid, false otherwise.</returns>
        private static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            using (
                new IndentedTraceGroup(
                    TraceSettings.CoppyVirrusWebbTraceLevelSwitch.TraceInfo,
                    $"GenericHelper: IsValidEmail: E-mail: \"{email}\"",
                    "/// GenericHelper: IsValidEmail"))
            {
                try
                {
                    var addr = new MailAddress(email);
                    return addr.Address == email;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
        }
    }
}
