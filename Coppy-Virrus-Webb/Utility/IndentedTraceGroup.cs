﻿// <copyright file="IndentedTraceGroup.cs" company="1.61">
// Copyright © 2018 1.61
// </copyright>

namespace Coppy_Virrus_Webb.Utility
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using Microsoft.Extensions.Logging;
    using Serilog;

    using static System.FormattableString;

    /// <summary>
    /// Upon creation writes to trace and indents trace. When destroyed - unindents. Use with using operator.
    /// </summary>
    public class IndentedTraceGroup : IDisposable
    {
        /// <summary>
        /// Threads are registered here.
        /// </summary>
        private static object[][] registeredThreads = new object[1][];

        /// <summary>
        /// Message to output when operation has ended.
        /// </summary>
        private string doneMessage;

        /// <summary>
        /// Trace switch condition. <see cref="Trace.WriteLineIf(bool, string)"/>.
        /// </summary>
        private bool traceCondition;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndentedTraceGroup"/> class.
        /// </summary>
        /// <param name="startMessage">
        /// Message to send to trace immediately. This will be formatted using Invariant culture.
        /// </param>
        /// <param name="doneMessage">
        /// Message to send to trace upon disposing.
        /// </param>
        /// <param name="logger">Application logger.</param>
        public IndentedTraceGroup(FormattableString startMessage, string doneMessage = "///")
        {
            this.Initialize(true, Invariant(startMessage), doneMessage);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndentedTraceGroup"/> class.
        /// </summary>
        /// <param name="traceCondition">The trace switch for use in tracing.</param>
        /// <param name="startMessage"> Message to send to trace immediately. This will be formatted using Invariant culture.</param>
        /// <param name="doneMessage">Message to send to trace upon disposing.</param>
        /// <param name="logger">Application logger.</param>
        public IndentedTraceGroup(bool traceCondition, FormattableString startMessage, string doneMessage = "///")
        {
            this.Initialize(traceCondition, Invariant(startMessage), doneMessage);
        }

        /// <summary>
        /// Implements Dispose Pattern.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndentedTraceGroup"/> class.
        /// </summary>
        /// <param name="initTraceCondition">The trace switch for use in tracing.</param>
        /// <param name="initStartMessage">Message to send to trace immediately.</param>
        /// <param name="initDoneMessage">Message to send to trace upon disposing.</param>
        protected void Initialize(bool initTraceCondition, string initStartMessage, string initDoneMessage)
        {
            this.traceCondition = initTraceCondition;
            this.doneMessage = initDoneMessage;

            if (!this.traceCondition)
            {
                return;
            }

            var lockTaken = false;
            try
            {
                Monitor.TryEnter(registeredThreads, new TimeSpan(0, 0, 0, 30), ref lockTaken);
                if (!lockTaken)
                {
                    throw new TimeoutException("Cannot obtain lock to write trace!");
                }

                this.WriteTrace(initTraceCondition, initStartMessage);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            finally
            {
                // Weird behavior when lockTaken==true and lock was not acquired.
                if (Monitor.IsEntered(registeredThreads))
                {
                    Debug.Assert(lockTaken, "weird!");
                    Monitor.Exit(registeredThreads);
                }
            }
        }

        /// <summary>
        /// Implements Dispose Pattern.
        /// WARNING! Thread indentations are kept forever!
        /// </summary>
        /// <param name="disposing">
        /// See Dispose Pattern.
        /// </param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Trace.WriteLine does not have exceptions thrown documented")]
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (!this.traceCondition)
            {
                return;
            }

            var lockTaken = false;
            Monitor.TryEnter(registeredThreads, new TimeSpan(0, 0, 0, 30), ref lockTaken);
            if (!lockTaken)
            {
                try
                {
                    ////Trace.WriteLine("Thread #" + Thread.CurrentThread.ManagedThreadId + ": Cannot obtain lock!");
                    ////Trace.WriteLineIf(this.traceCondition, "Thread #" + Thread.CurrentThread.ManagedThreadId + ": " + this.doneMessage);
                    Log.Information("Thread #" + Thread.CurrentThread.ManagedThreadId + ": Cannot obtain lock!");
                    if (this.traceCondition)
                    {
                        Log.Information("Thread #" + Thread.CurrentThread.ManagedThreadId + ": " + this.doneMessage);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, ex.Message);
                }
            }

            try
            {
                // Strange bug.
                if (registeredThreads == null)
                {
                    return;
                }

                var indexThread = Array.IndexOf(
                    registeredThreads,
                    Array.Find(
                        registeredThreads,
                        t => t != null && (int)t[0] == Thread.CurrentThread.ManagedThreadId));

                // WARNING! Exception here! (Wrong workaround added.)
                if (registeredThreads[indexThread] == null)
                {
                    registeredThreads[indexThread] = new object[] { Thread.CurrentThread.ManagedThreadId, 1 };
                }

                var indentLevel = (int)registeredThreads[indexThread][1];
                indentLevel--;
                registeredThreads[indexThread][1] = indentLevel;
                Log.Information("Thread #" + Thread.CurrentThread.ManagedThreadId + ": " + this.doneMessage);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
            finally
            {
                // Weird behavior when lockTaken==true and lock was not acquired.
                if (Monitor.IsEntered(registeredThreads))
                {
                    Debug.Assert(lockTaken, "weird!");
                    Monitor.Exit(registeredThreads);
                }

                //// Log.CloseAndFlush();
            }
        }

        /// <summary>
        /// Writes trace message keeping indentation separate for each thread.
        /// </summary>
        /// <param name="initTraceCondition">Condition of when to write</param>
        /// <param name="initStartMessage">Message to write</param>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This is not necessary")]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Trace.WriteLine does not have exceptions thrown documented")]
        private void WriteTrace(bool initTraceCondition, string initStartMessage)
        {
            if (string.IsNullOrWhiteSpace(initStartMessage))
            {
                throw new ArgumentNullException(nameof(initStartMessage));
            }

            try
            {
                var threadExists = Array.Exists(
                    registeredThreads,
                    t => t != null && Array.Exists(t, inner => (int)inner == Thread.CurrentThread.ManagedThreadId));

                int indexThread = 0;
                var indentLevel = 0;

                if (threadExists)
                {
                    indexThread = Array.IndexOf(
                        registeredThreads,
                        Array.Find(registeredThreads, t => t != null && (int)t[0] == Thread.CurrentThread.ManagedThreadId));
                    indentLevel = (int)registeredThreads[indexThread][1];
                }
                else
                {
                    if (registeredThreads?.Length != null)
                    {
                        var newLength = (int)registeredThreads?.Length + 1;
                        indexThread = newLength - 1;
                        Array.Resize(ref registeredThreads, newLength);
                    }

                    if (registeredThreads != null)
                    {
                        registeredThreads[indexThread] = new object[] { Thread.CurrentThread.ManagedThreadId, 0 };
                    }
                }

                Log.Information("Thread #" + Thread.CurrentThread.ManagedThreadId + ": " + initStartMessage);

                indentLevel++;
                if (registeredThreads != null)
                {
                    registeredThreads[indexThread][1] = indentLevel;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Log.Information("Thread #" + Thread.CurrentThread.ManagedThreadId + ": " + initStartMessage);
                }
                catch
                {
                    // ignored
                }
            }
        }
    }
}
