﻿// <copyright file="TraceSettings.cs" company="1.61">
// Copyright © 2018 1.61
// </copyright>

namespace Coppy_Virrus_Webb.Utility
{
    using System.Diagnostics;

    /// <summary>
    /// Static configuration settings for tracing.
    /// </summary>
    internal static class TraceSettings
    {
        /// <summary>
        /// Gets general trace level taken from the .config file.
        /// </summary>
        internal static TraceSwitch CoppyVirrusWebbTraceLevelSwitch => new TraceSwitch("CoppyVirrusWebbSwitch", "General switch.", "Info");
    }
}