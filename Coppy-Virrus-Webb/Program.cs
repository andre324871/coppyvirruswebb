﻿namespace Coppy_Virrus_Webb
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.AccessControl;
    using System.Threading.Tasks;

    using Coppy_Virrus_Webb.Utility;

    using Microsoft.Extensions.Configuration;

    using Serilog;

    class Program
    {
        private static IConfiguration config;

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File(Directory.GetCurrentDirectory() + @"\\Logs\\log.txt")
                .CreateLogger();

            Log.Information("Application started...");

            try
            {
                var file = File.CreateText(Directory.GetCurrentDirectory() + @"\\Logs\\Serilog.log");
                Serilog.Debugging.SelfLog.Enable(TextWriter.Synchronized(file));
                config = new ConfigurationBuilder().AddJsonFile(
                        "appsettings.json",
                        optional: false,
                        reloadOnChange: true).Build();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, ex.Message);
                throw;
            }

            var conDict = config.ConfigToDictionary();
            var webFolders = config.GetSection("WebFolders");
            var domain = conDict["Domain"];
            var owners = GenericHelper.ConfigSectionToList(config.GetSection("Owners"));
            var virusNames = GenericHelper.ConfigSectionToList(config.GetSection("VirusNames"));
            var i = 0;
            var j = 0;
            var k = 0;
            foreach (var vrsName in Directory.GetFiles(conDict["Folder"]))
            {
                string webFolder;
                string webName;

                if (i == webFolders.GetChildren().Count())
                {
                    i = 0;
                }

                if (j == owners.Count)
                {
                    j = 0;
                }

                if (k == virusNames.Count)
                {
                    k = 0;
                }

                try
                {
                    webFolder = webFolders.GetChildren().ToArray()[i]["WebFolder"];
                    webName = webFolders.GetChildren().ToArray()[i]["Web"];
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex, ex.Message);
                    throw;
                }

                var fileName = Path.GetFileName(vrsName);
                var destFile = Path.Combine(webFolder, virusNames[k] + ".exe");

                try
                {
                    if (File.Exists(destFile))
                    {
                        File.Delete(destFile);
                    }

                    File.Copy(vrsName, destFile);
                    FileSecurity fs = new FileSecurity(destFile, AccessControlSections.Owner);
                    fs.SetOwner(new System.Security.Principal.NTAccount(domain, owners[j]));

                    // Create a new FileInfo object.
                    FileInfo fInfo = new FileInfo(destFile);

                    // Set the new access settings.
                    fInfo.SetAccessControl(fs);
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex, ex.Message);
                    throw;
                }

                i++;
                j++;
                k++;

                CoppyHelper.RunScript(webFolder, webName, fileName);
            }

            Log.Information("Application ended...");
        }
    }
}
